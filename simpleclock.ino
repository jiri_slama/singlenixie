// single nixie clock v1

#include "Wire.h"
#define DS3231_I2C_ADDRESS 0x68
#define BLANK  15   // BCD value for blank display
#define COMMIT 13   // BCD value for RTC-write indication (only for hp5082)
#define MODE_BUT 3
#define SET_BUT  4

byte ss, mm, hh, temp;  // rtc values
byte h1, h0, m1, m0;    // digits values

byte mode = 0;  // "mode" button state
byte setb = 0;  // "set" button state
byte state = 0; // clock state

void setup(){
    Wire.begin();

    for (int i=8; i<=11; i++) pinMode(i, OUTPUT); // BCD output is on d8..d11
    pinMode(MODE_BUT, INPUT);
    pinMode(SET_BUT, INPUT);

    attachInterrupt(digitalPinToInterrupt(MODE_BUT), mode_change, RISING);
}

void mode_change () {
    mode = 1;
}

void setDS3231time(byte second, byte minute, byte hour){
    Wire.beginTransmission(DS3231_I2C_ADDRESS);
    Wire.write(0);
    Wire.write(second);
    Wire.write(minute);
    Wire.write(hour);
    Wire.endTransmission();
}

void readDS3231time(byte *second, byte *minute, byte *hour){
    Wire.beginTransmission(DS3231_I2C_ADDRESS);
    Wire.write(0);
    Wire.endTransmission();
    Wire.requestFrom(DS3231_I2C_ADDRESS, 3);
    *second = Wire.read();
    *minute = Wire.read();
    *hour = Wire.read() & 0x3f;
}

void readDS3231temp(byte *temp){
    Wire.beginTransmission(DS3231_I2C_ADDRESS);
    Wire.write(0x11);
    Wire.endTransmission();
    Wire.requestFrom(DS3231_I2C_ADDRESS, 1);
    *temp = Wire.read();
}

void disp (byte i, int delay_on, int delay_off) {
    PORTB = i;
    delay(delay_on);
    PORTB = BLANK;
    delay(delay_off);
}

void dispTime () {
    readDS3231time(&ss, &mm, &hh);
    
    h1 = hh >> 4;
    h0 = hh & 0x0f;
    m1 = mm >> 4;
    m0 = mm & 0x0f;
    
    setb |= !digitalRead(SET_BUT);
    disp(h1,500,200);
    setb |= !digitalRead(SET_BUT);
    disp(h0,500,800);
    setb |= !digitalRead(SET_BUT);
    disp(m1,500,200);
    setb |= !digitalRead(SET_BUT);
    disp(m0,500,1200);
    setb |= !digitalRead(SET_BUT);
}

void dispTemp () {
    readDS3231temp(&temp);

    disp(temp/10,500,200);
    disp(temp%10,500,1200);
}

void loop() {
    if (mode) {
        state = (state+1)%6;
        delay(500);
    }

    mode = 0;
    setb = !digitalRead(SET_BUT);

    switch (state) {
        case 0: dispTime();
                if (setb) dispTemp();
                break;

        case 1: if (setb) { h1 = (h1 + 1)%3; }
                disp(h1,100,100);
                break;

        case 2: if (setb) { h0 = (h0 + 1)%10; }
                if (h1*10 + h0 > 23) h0 = 0;
                disp(h0,100,100);
                break;

        case 3: if (setb) { m1 = (m1 + 1)%6; }
                disp(m1,100,100);
                break;

        case 4: if (setb) { m0 = (m0 + 1)%10; }
                disp(m0,100,100);
                break;

        case 5: // write RTC and go!
                hh = (h1<<4) + h0;
                mm = (m1<<4) + m0;
                ss = 0;
                setDS3231time(ss, mm, hh);
        default:
                state = 0;
                disp(COMMIT,500,100);
                break;
    }

    setb = 0;
    delay(100);
}
